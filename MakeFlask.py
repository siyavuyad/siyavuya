from flask import Flask, send_from_directory, make_response
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/example', methods = ['GET'])
@cross_origin() 
def example():
    return {'hello': 'world'}

if __name__ == "__main__":
    app.run(host = '0.0.0.0', debug=True)