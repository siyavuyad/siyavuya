import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPythonComponent } from './show-python.component';

describe('ShowPythonComponent', () => {
  let component: ShowPythonComponent;
  let fixture: ComponentFixture<ShowPythonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPythonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPythonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
