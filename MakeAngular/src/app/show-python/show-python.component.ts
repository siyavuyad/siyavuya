import { Component, OnInit } from '@angular/core';
import { FlaskServiceService } from '../flask-service.service';

@Component({
  selector: 'app-show-python',
  templateUrl: './show-python.component.html',
  styleUrls: ['./show-python.component.css']
})
export class ShowPythonComponent implements OnInit {
  thingy: any
  constructor(private flask: FlaskServiceService) { }

  ngOnInit(): void {
  }

showFlaskResponse() {
  this.flask.getExample().toPromise().then(response => {
    this.thingy = response
  }).catch(error => console.error(error));
}
}
