import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FlaskServiceService {

  constructor(private httpClient: HttpClient) { }

  getExample(): Observable<any> {
    let endpoint = environment.api + '/example';
    return this.httpClient.get(endpoint);
  }
}
